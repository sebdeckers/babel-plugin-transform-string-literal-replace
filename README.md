# string-literal-replace

A simple Babel plugin to transform source code by finding and replacing string literals with other values.

## Use Case

Inject constants and other snippets into source code.

## Usage

### `.babelrc`

```json
{
  "plugins": [
    ["transform-string-literal-replace", {
      "Sebastiaan": "Seb",
      "SOME_MESSAGE": 1234567890
    }]
  ]
}
```

```js
import {transform} from 'babel-core'
const input = `var name = 'Sebastiaan';alert('SOME_MESSAGE');`
const output = transform(input)
// -> `var name = 'Seb';alert(1234567890);`
```

## Options

### `pattern`

An `Object` or `Map` containing strings to replace with values that are either `String`, `Number`, `Boolean`, or `null`.
