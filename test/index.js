import test from 'ava'
import {transform} from 'babel-core'
import stringLiteralReplace from '..'

const fixtures = [
  {
    label: 'Replace with a string',
    given: `;'foo';`,
    patterns: {foo: 'bar'},
    expected: `;'bar';`
  },
  {
    label: 'Replace with a number',
    given: `;'foo';`,
    patterns: {foo: 123},
    expected: `;123;`
  },
  {
    label: 'Replace with a null',
    given: `;'foo';`,
    patterns: {foo: null},
    expected: `;null;`
  },
  {
    label: 'Replace with a boolean',
    given: `;'foo';`,
    patterns: {foo: true},
    expected: `;true;`
  },
  {
    label: 'Map as patterns',
    given: `;'foo';`,
    patterns: new Map([['foo', 'bar']]),
    expected: `;'bar';`
  },
  {
    label: 'README usage example',
    given: `var name = 'Sebastiaan';alert('SOME_MESSAGE');`,
    patterns: {Sebastiaan: 'Seb', SOME_MESSAGE: 1234567890},
    expected: `var name = 'Seb';alert(1234567890);`
  }
]

for (const {label, given, patterns, expected} of fixtures) {
  test(label, (t) => {
    const actual = transform(given, {
      plugins: [
        [stringLiteralReplace, {patterns}]
      ]
    })
    t.is(actual.code, expected)
  })
}
