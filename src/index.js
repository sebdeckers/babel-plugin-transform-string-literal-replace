function has (patterns, pattern) {
  return (typeof patterns.has === 'function') ? patterns.has(pattern)
    : patterns::Object.prototype.hasOwnProperty(pattern)
}

function get (patterns, pattern) {
  return (typeof patterns.get === 'function') ? patterns.get(pattern)
    : patterns[pattern]
}

function output (t, result) {
  return (typeof result === 'string') ? t.stringLiteral(result)
    : (typeof result === 'number') ? t.numericLiteral(result)
    : (typeof result === 'boolean') ? t.booleanLiteral(result)
    : (result === null) ? t.nullLiteral()
    : result
}

function stringLiteralReplace ({types: t}) {
  return {
    visitor: {
      StringLiteral (path, state) {
        const patterns = state.opts.patterns
        const value = path.node.value
        if (!has(patterns, value)) return
        const result = get(patterns, value)
        path.replaceWith(output(t, result))
      }
    }
  }
}

module.exports = stringLiteralReplace
